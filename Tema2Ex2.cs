﻿using System;
class TwoGen<T, V>
{
    T ob1;
    V ob2;

    public TwoGen(T o1, V o2)
    {
        ob1 = o1;
        ob2 = o2;
    }

    public void showTypes()
    {
        Console.WriteLine("Type of T is " + typeof(T));
        Console.WriteLine("Type of V is " + typeof(V));
    }

    public T getob1()
    {
        return ob1;
    }

    public V GetObj2()
    {
        return ob2;
    }
}

public class Program
{
    static void Main()
    {

        TwoGen<int, string> tgObj =
          new TwoGen<int, string>(123, "This is a test");

        tgObj.showTypes();

        int v = tgObj.getob1();
        Console.WriteLine("value: " + v);
        string str = tgObj.GetObj2();
        Console.WriteLine("value: " + str);
    }
}
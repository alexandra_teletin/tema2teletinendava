﻿using System;

//TELETIN ALEXANDRA EX 1
namespace Reverse
{
    class Program
    {
        static void Main(string[] args)
        {
            Test<int>(new int[] { 1, 2, 3, 4, 5 });
            Test<string>(new string[] { "A", "L", "E", "X", "A", "N", "D","R","A" });

            Console.ReadLine();
        }
        static void Test<T>(T[] array)
        {
            Write(array);
            array = Reverse<T>(array);
            Write(array);
            Console.WriteLine();
        }
        static void Write<T>(T[] array)
        {
            if (array != null)
            {
                foreach (T i in array)
                {
                    Console.Write("{0} ", i);
                }
            }
            Console.WriteLine();
        }
        static T[] Reverse<T>(T[] array)
        {
            T[] newArray = null;
            int count = array == null ? 0 : array.Length;
            if (count > 0)
            {
                newArray = new T[count];
                for (int i = 0, j = count - 1; i < count; i++, j--)
                {
                    newArray[i] = array[j];
                }
            }
            return newArray;
        }
    }
}